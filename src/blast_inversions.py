# import things

import argparse
from Bio.Blast.Applications import NcbiblastnCommandline
from Bio import SeqIO
import os.path


parser = argparse.ArgumentParser(description="""Blasts flanking regions of structural variants and returns file with
                                                alignment info.""")
parser.add_argument("-i", "--input", required = True, help = "Input csv with validated clustered svs")
parser.add_argument("-s", "--isolates", required = True, help = "Input file with all isolates included in study")
parser.add_argument("-o", "--output", required = False, default='blast_output.tsv', help = "user defined name for output"
                                                                        " file. Default is blast_output.tsv")
parser.add_argument("-e", "--error_output", required = False, default='errors_blast_output.csv', help = "lines from the input "
                                                                        "file where the corresponding regions could not"
                                                                        " be run through BLAST,")
args = parser.parse_args()
in_csv = args.input
in_iso_file = args.isolates
output_name = args.output
error_output_name = args.error_output

# for each SV, get the query sequence from the representative isolate
# read in validated clustered svs file, pull isolate (ID) and SV_ID
# use get_fasta function to get consensus fasta for the isolate
# blast subject = consensus sequence, query = the sv sequence plus 250 before and after the breakpoints

# manual_representatives = {('1', 'DUP'):'2-0034', ('2','DUP'):'1-0044', ('3', 'DUP'):'1-0045',('4', 'DUP'):'1-0168',
#                           ('5','DUP'):'N0157',('6','DEL'):'1-0138',('7','DEL'):'N1063',
#                           ('8', 'DEL'):'2-0043',('9', 'DEL'):'N1063',('10', 'DEL'):'1-0142',('11', 'DEL'):'N0157',
#                           ('12', 'DEL'):'N0157', ('13', 'INV'):'N1283', ('14', 'INV'):'1-0019', ('15', 'INV'):'1-0070',
#                           ('16', 'INV'): '1-0036', ('17', 'INV'):'N1063', ('18','INV'):'1-0109'}
manual_representatives = {('13', 'INV'):'N1283', ('14', 'INV'):'1-0019', ('15', 'INV'):'1-0070',
                          ('16', 'INV'): '1-0036', ('17', 'INV'):'N1063', ('18','INV'):'1-0109'}


def get_qloc_dict(sv_file):
    qloc_dict={}
    with open(sv_file) as sv_isolates:
        for line in sv_isolates:
            if not line.startswith('cluster_ID'):
                line = line.strip().split(',')
                isolate = line[1]
                if isolate == '2-0043-unknown':
                    isolate = '2-0043'
                sv_id = line[12]
                sv_type = line[4]
                start = line[2]
                svlen = line[3]
                end = int(start)+int(svlen)
                if sv_type == 'DUP' or sv_type == 'DEL':
                    qstart=int(start)-250
                    qend = end+250
                    qloc_dict[(1, isolate, sv_id)] = str(qstart) + '-' + str(qend)
                elif sv_type == 'INV':
                    qstart1 = int(start)-500
                    qend1 = int(start)+500
                    qloc_dict[(1, isolate, sv_id)] = str(qstart1) + '-' + str(qend1)
                    qstart2 = int(end)-500
                    qend2 = int(end)+500
                    qloc_dict[(2, isolate, sv_id)] = str(qstart2) + '-' + str(qend2)
                else:
                    print("SV type in the following line isn't DUP, DEL, or INV")
                    print(str(line))
    return qloc_dict


def get_full_iso_list(iso_file):
    iso_list = []
    with open(iso_file) as isolate_info:
        for line in isolate_info:
            if not line.startswith('isolate'):
                line = line.strip().split(',')
                isolate = line[0]
                iso_list.append(isolate)
    return iso_list


def blast(subject_fasta,query_fasta,qloc):
    """Takes location of 2 sequences in a fasta file (format: 'start-stop'). Runs BLAST on the 2 sequences.
    Returns a list of all BLAST runs."""
    blast_command = NcbiblastnCommandline(subject=subject_fasta, query=query_fasta, query_loc=qloc,
                                          outfmt='6')
    stdout, stderr = blast_command()
    return stdout


def get_fasta(isolate_name):
    if os.path.isfile('/grp/valafar/data/genomes/'+isolate_name+'.fasta'):
        fasta = '/grp/valafar/data/genomes/'+isolate_name+'.fasta'
    elif os.path.isfile('/grp/valafar/data/variants/sniffles-sv/het/new_isos/'+isolate_name+'.fasta'):
        fasta = '/grp/valafar/data/variants/sniffles-sv/het/new_isos/'+isolate_name+'.fasta'
    else:
        print('Fasta for isolate ' + isolate_name + ' not found in genomes/ directory or new_isos/ directory')
    return fasta

def get_sequence(fasta_file,seq_location):
    """Gets sequence from fasta file. Uses first record in fasta only.
    Intended to be used when qloc blast command doesn't
    work. Input the sequence in blast instead."""
    record_count = 0
    loc1 = int(seq_location.split('-')[0])
    loc2 = int(seq_location.split('-')[1])
    for record in SeqIO.parse(fasta_file, "fasta"):
        record_count+=1
        if record_count == 1:
            extracted_sequence = str(record.seq[loc1:loc2])
    return extracted_sequence


# This function differs from the regular blast script.
# Inversions only file includes qloc1 and qloc2 and all blast results rather than top hit
# Output file has two extra columns (breakpoint 1 or 2 and blast hit count
def iterate(qloc_dict, representative_dict, full_iso_list):
    """."""
    all_blast_results = []
    errors = []
    for sv,svtype in representative_dict:
        qisolate = representative_dict[(sv,svtype)]
        query_fasta_file = get_fasta(qisolate)
        qloc1 = qloc_dict[(1, qisolate, sv)]
        # subset full_iso_list for testing
        for sisolate in full_iso_list:
            subject_fasta_file = get_fasta(sisolate)
            # BREAKPOINT 1 #
            try:
                blast_list = blast(subject_fasta_file, query_fasta_file, qloc1).split('\n')[:-1]
            except Exception as e:
                print('EXCEPTION for blasting {} location {} against subject {}'.format(qisolate, qloc1, sisolate))
                blast_list = ['NA']*12
                errors.append(','.join([qisolate, qloc1, sisolate,str(e)]))
            hit_counter1 = 0
            for hit in blast_list:
                hit_counter1 += 1
                hit = hit.split('\t')[2:]
                file_string1 = '1'+'\t'+str(hit_counter1)+'\t'+sv+'\t'+qisolate+'\t'+qloc1 +'\t'+sisolate+'\t'+'\t'.join(hit)
                all_blast_results.append(file_string1)
            # BREAKPOINT 2 #
            qloc2 = qloc_dict[(2, qisolate, sv)]
            try:
                blast_list = blast(subject_fasta_file, query_fasta_file, qloc2).split('\n')[:-1]
            except Exception as e:
                blast_list = ['NA'] * 12
                errors.append(','.join([qisolate, qloc2, sisolate, str(e)]))
            hit_counter2 = 0
            for hit in blast_list:
                hit_counter2 += 1
                hit = hit.split('\t')[2:]
                file_string2 = '2' + '\t' + str(
                    hit_counter2) + '\t' + sv + '\t' + qisolate + '\t' + qloc2 + '\t' + sisolate + '\t' + '\t'.join(
                    hit)
                all_blast_results.append(file_string2)
    return all_blast_results, errors


def write_blast_output(blast_list):
    header = 'breakpoint\tblast_hit_order\tSVID\trep_ISOLATE\trep_LOCATION\tconsensus_ISOLATE\tpident\tlength\tmismatch\t' \
             'gapopen\tqstart\tqend\tsstart\tsend\tevalue\tbitscore\n'
    blast_string = '\n'.join(blast_list)
    file_string = header + blast_string + '\n'
    with open(output_name, 'w') as out_file:
        out_file.write(file_string)


def write_errors(errors, error_output_name):
    header = 'qisolate,qloc,sisolate,error\n'
    error_string = '\n'.join(errors)
    file_string = header + error_string
    with open(error_output_name, 'w') as out_file:
        out_file.write(file_string)


if __name__ == "__main__":
    qloc_dict = get_qloc_dict(in_csv)
    full_iso_list = get_full_iso_list(in_iso_file)
    blast_list, errors = iterate(qloc_dict,manual_representatives, full_iso_list)
    write_errors(errors, error_output_name)
    write_blast_output(blast_list)

