import argparse

parser = argparse.ArgumentParser(description="""Calculates length of sequences for a given list of isolate fastas.""")
parser.add_argument("-i", "--input", required=True, help="fofn file with all fasta file names to be included")
parser.add_argument("-o", "--output", required=True, help="Name of output csv file")

args = parser.parse_args()
isolate_fofn = args.input
csv_out = args.output


def extract_filenames(input_file):
    out_list = []
    with open(input_file, 'r') as open_file:
        for line in open_file:
            line = line.strip()
            out_list.append(line)
    return out_list


def get_length(isolate_file):
    with open(isolate_file, 'r') as file:
        seq_str = ''
        for line in file:
            if not line.startswith('>'):
                line = line.strip()
                seq_str += line
    return len(seq_str)


def write_length(ouputname, iso, isolength):
    file_string = iso + ',' + str(isolength) + '\n'
    with open(ouputname, 'a') as csv_open:
        csv_open.write(file_string)
    return csv_open


def write_all():
    isolate_files = extract_filenames(isolate_fofn)
    for isolate_path in isolate_files:
        isolate = isolate_path.split('/')[-1].split('.')[0]
        # print(isolate)
        try:
            length = get_length(isolate_path)
            write_length(csv_out, isolate, length)
        except:
            print('permission for' + isolate + ' denied')


if __name__ == '__main__':
    write_all()
