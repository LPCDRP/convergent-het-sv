while read p; do
  python alignment_validation.py -i "$p" -o ../align_out.tsv -e ../error_reads.csv
done <$GROUPHOME/data/variants/sniffles-sv/comparative_sv_seqs.csv 
