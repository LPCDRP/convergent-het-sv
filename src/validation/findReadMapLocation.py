#!/usr/bin/python3

import pysam


def parse_SVS_data(input_row):
    line = input_row.strip().split(",")
    sv_id = line[0]
    isolate = line[1]
    read_name = line[5]
    bp_1 = line[2]
    sv_type = line[4]
    bp_2 = line[3]
    SV_read_info = [isolate, sv_id, read_name, sv_type, bp_1, bp_2]
    return SV_read_info


def get_parsed_alignments(path_to_bams, iso_read_dict):
    output_dict = {}
    for isolate in iso_read_dict:
        read_list = iso_read_dict[isolate]
        bam_file = pysam.AlignmentFile(path_to_bams + isolate + ".bam", "rb")
        for alignment in bam_file:
            if alignment.qname in read_list:
                A_start = alignment.reference_start
                A_stop = alignment.reference_end
                qry_start = alignment.qstart
                qry_stop = alignment.qend
                R_seq = alignment.query_alignment_sequence
                if alignment.is_reverse:
                    strand = '-'
                else:
                    strand = '+'
                alignment_list = [(A_start, A_stop), strand, (qry_start, qry_stop), R_seq]
                output_dict[(alignment.qname, A_start)] = alignment_list
    return output_dict


def get_unique_reads(SV_location_csv):
    unique_dict = {}
    with open(SV_location_csv, "r") as read_obj:
        for SV in read_obj:
            line = SV.strip().split(",")
            isolate = line[1]
            readname = line[5]
            if isolate not in unique_dict:
                unique_dict[isolate] = []
            unique_dict[isolate].append(readname)
    return unique_dict
