import argparse
import os.path
# from Bio import pairwise2
from Bio import SeqIO
from Bio.Seq import Seq
from Bio import Align
import numpy as np


def calculate_B_regions(A1, A2, svtype, A1strand, A2strand):
    a1length = int(A1[1]) - int(A1[0])
    a2length = int(A2[1]) - int(A2[0])
    if svtype == 'DEL' or svtype == 'DUP':
        B1 = (int(A2[0])-a1length, A2[0])
        B2 = (A1[1], int(A1[1])+a2length)
    elif svtype == 'INV':
        if A1strand == '+':
            B1 = (A2[1], int(A2[1])+a1length)
            B2 = (A1[1], int(A1[1])+a2length)
        elif A1strand == '-':
            B1 = (int(A2[0])-a1length, A2[0])
            B2 = (int(A1[0])-a2length, A1[0])
    return B1, B2


def get_fasta(isolate_name):
    if os.path.isfile('/grp/valafar/data/genomes/'+isolate_name+'.fasta'):
        fasta = '/grp/valafar/data/genomes/'+isolate_name+'.fasta'
    elif os.path.isfile('/grp/valafar/data/variants/sniffles-sv/het/new_isos/'+isolate_name+'.fasta'):
        fasta = '/grp/valafar/data/variants/sniffles-sv/het/new_isos/'+isolate_name+'.fasta'
    else:
        print('Fasta for isolate ' + isolate_name + ' not found in genomes/ directory or new_isos/ directory')
    return fasta


def get_sequence(region, fasta, strand):
    """Takes a tuple with indexes for the A or B region, a fasta file name, and the strand to pull the sequence from
    ('+' or '-'). If minus strand is given, the pulled sequence is reversed and complimented.
    Returns the desired sequence from the fasta as a string."""
    record = list(SeqIO.parse(fasta, "fasta"))[0]
    start = int(region[0])
    stop = int(region[1])
    ab_seq = str(record.seq)[start:stop]
    if strand == '-':
        ab_seq = str(Seq(ab_seq).reverse_complement())
    return ab_seq


def align(ref_string, read_string, match = 1, mismatch = -1, gap_score = -2):
    """Takes 2 strings, performs global alignment. Can specify scores for match, mismatch, and gap score.
     Default values are 1, -1, and -2, respectively. When using these values, Needleman-Wunsch algorithm
     is automatically implemented. If there are N chunks of alignment (not gaps) between the two sequences,
     function will return two tuples of length N. First tuple contains reference chunks and second tuple
     contains read chunks."""
    aligner = Align.PairwiseAligner()
    aligner.mode = 'global'
    aligner.match_score = match
    aligner.mismatch_score = mismatch
    aligner.target_gap_score = gap_score
    aligner.query_gap_score = gap_score
    alignments = aligner.align(ref_string, read_string)
    alignment = alignments[0]
    return alignment.aligned


def make_read_ref_dict(aligned_tuples):
    """Takes the tuples output from the align function (first tuple contains reference aligned chunks
    and second contains read aligned chunks). Outputs a dictionary with read position as key and reference
    position that aligns as value."""
    read_ref_dict = {}
    ref_tups, read_tups = aligned_tuples[0], aligned_tuples[1]
    chunk_number = 0
    for read_tup in read_tups:
        ref_tup = ref_tups[chunk_number]
        ref_list = [pos for pos in range(ref_tup[0], ref_tup[1])]
        ref_list_index = 0
        for i in range(read_tup[0], read_tup[1]):
            read_ref_dict[i] = ref_list[ref_list_index]
            ref_list_index += 1
        chunk_number += 1
    return read_ref_dict

# A1_aligned = (((0, 2), (2, 6)), ((0, 2), (3, 7)))
# B1_aligned = (((0, 7), (7, 37)), ((0, 7), (8, 38)))
# A1seq = 'TCATAC'
# R1seq = 'TCGAGGC'
# B1seq = 'CATACGC'


def get_n_k_values(A_aligned, B_aligned, Aseq, Bseq, Rseq):
    n, k_a, k_b = 0, 0, 0
    A_dict = make_read_ref_dict(A_aligned)
    B_dict = make_read_ref_dict(B_aligned)
    # print(A_dict, B_dict)
    # print('Length of A dict: ' + str(len(A_dict)))
    # print('Length of B dict: ' + str(len(B_dict)))
    for read_pos in A_dict:
        if read_pos in B_dict:
            R_base = Rseq[read_pos]
            A_pos, B_pos = A_dict[read_pos], B_dict[read_pos]
            A_base, B_base = Aseq[A_pos], Bseq[B_pos]
            # print('A = ' + A_base)
            # print('R = ' + R_base)
            # print('B = ' + B_base)
            if A_base != B_base:
                n += 1
                # print ('Adding 1 to n')
                if A_base == R_base:
                    k_a += 1
                    # print('Adding 1 to k_a')
                if B_base == R_base:
                    k_b += 1
                    # print('Adding 1 to k_b')
    return [n, k_a, k_b]
# 4, 2, 1
