import argparse
import findReadMapLocation as read_maps
import alignment_validation as alignment

parser = argparse.ArgumentParser(description="""""")
parser.add_argument("-i", "--input", required = False, default = "/grp/valafar/data/variants/sniffles-sv/"
                                        "convergentsv_readnames.csv", help = "Input CSV file containing SV info with "
                                        "readnames. Each line represents one read that supports a structural variant.")
parser.add_argument("-o", "--output", required = False, default="../alignment_n_K_values.csv", help = "name of output"
                                                                " file. Default is ../alignment_n_K_values.csv")
parser.add_argument("-b", "--bam_path", required = False, default = "/grp/valafar/data/variants/sniffles-sv/"
                                                                "het-reads/bams/", help= "path to directory where "
                                                                "bam files from ngmlr (for all isolates) are located.")


args = parser.parse_args()
input_sv_readnames = args.input
output_name = args.output
bam_path = args.bam_path


match_score = 1
mismatch_score = -1
gap_penalty = -2


def main():
    file_string = ''
    iso_read_dict = read_maps.get_unique_reads(input_sv_readnames)
    # test_dict = {'1-0007': iso_read_dict['1-0007']}
    alignment_dict = read_maps.get_parsed_alignments(bam_path, iso_read_dict)
    with open(input_sv_readnames, 'r') as sv_readnames:
        # for row in sv_readnames.readlines()[0:3]:
        for row in sv_readnames.readlines():
            sv_info = read_maps.parse_SVS_data(row)
            isolate = sv_info[0]
            sv_type = sv_info[3]
            readname = sv_info[2]
            total_maps = 0
            read_alignments = []
            for key in alignment_dict:
                if readname in key:
                    total_maps += 1
                    read_alignments.append(alignment_dict[key])
            if total_maps != 2:
                continue
        # manually look at duplications to make sure they are overlapping
            if sv_type == 'DEL' or sv_type == 'DUP':
                start_index = 0
                min_start = min(x[0][0] for x in read_alignments)
            elif sv_type == 'INV':
                start_index = 2
                min_start = min(x[2][0] for x in read_alignments)
            for align_info in read_alignments:
                if align_info[start_index][0] == min_start:
                    A1 = align_info[0]
                    R1seq = align_info[3]
                    A1strand = align_info[1]
                else:
                    A2 = align_info[0]
                    R2seq = align_info[3]
                    A2strand = align_info[1]
            B1, B2 = alignment.calculate_B_regions(A1, A2, sv_type, A1strand, A2strand)
            # check out B regions with a couple tests (mostly inversion check)
            fasta = alignment.get_fasta(isolate)
            A1_seq = alignment.get_sequence(A1, fasta, A1strand)
            A2_seq = alignment.get_sequence(A2, fasta, A2strand)
            if A1strand == A2strand:
                B1_seq = alignment.get_sequence(B1, fasta, A1strand)
                B2_seq = alignment.get_sequence(B2, fasta, A2strand)
            elif A1strand != A2strand:
                B1_seq = alignment.get_sequence(B1, fasta, A2strand)
                B2_seq = alignment.get_sequence(B2, fasta, A1strand)
            A1_aligned = alignment.align(A1_seq, R1seq)
            A2_aligned = alignment.align(A2_seq, R2seq)
            B1_aligned = alignment.align(B1_seq, R1seq)
            B2_aligned = alignment.align(B2_seq, R2seq)
            n_k_1 = alignment.get_n_k_values(A1_aligned, B1_aligned, A1_seq, B1_seq, R1seq)
            n_k_2 = alignment.get_n_k_values(A2_aligned, B2_aligned, A2_seq, B2_seq, R2seq)
            A1 = str(A1[0])+':'+str(A1[1])
            A2 = str(A2[0])+':'+str(A2[1])
            B1 = str(B1[0])+':'+str(B1[1])
            B2 = str(B2[0])+':'+str(B2[1])
            row1 = sv_info + ['1', A1, B1] + n_k_1
            row2 = sv_info + ['2', A2, B2] + n_k_2
            file_string += ','.join(map(str, row1)) + '\n' + ','.join(map(str, row2)) + '\n'
            write_file(file_string, output_name)


def write_file(filestring, output):
    header = 'isolate, sv_id, read_name, sv_type, bp_1, bp_2, alignment, A_region, B_region, n, k_A, k_B\n'
    with open(output, 'w') as open_csv:
        open_csv.write(header)
        open_csv.write(filestring)


if __name__ == "__main__":
    main()
