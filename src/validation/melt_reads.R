library(data.table)
library(dplyr)
library(tidyr)
# read input from command line
args <- commandArgs(trailingOnly = TRUE)
all <- args[1]
conv <- args[2]
output_name <- args[3]

all_svs <- fread(file=all)
convergent_svs <- fread(file=conv)

all_svs$SVLEN <- abs(all_svs$SVLEN)
convergent_svs <- inner_join(all_svs, convergent_svs, by = c('ID', 'POS', 'SVLEN')) %>%
  select(SV_ID, ID, POS, END, SVTYPE.x, RNAMES)

reads <- convergent_svs %>% 
  mutate(RNAMES = strsplit(as.character(RNAMES), ",")) %>%
  unnest(RNAMES)

#colnames(reads) <- c('SV_ID', 'ISOLATE', 'BP.POS', 'BP.END', "SVTYPE", 'READ.NAME')
fwrite(reads, file = output_name, col.names = FALSE
