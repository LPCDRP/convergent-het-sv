import alignment_validation as alignment
# import findReadMapLocation as read_maps
import argparse
import random
import pysam
# from Bio import Align
from functools import partial
from statistics import mean
import multiprocessing as mp
from datetime import datetime
startTime = datetime.now()

parser = argparse.ArgumentParser(description="""""")
parser.add_argument("-b", "--bam_path", required = False, default = "/grp/valafar/data/variants/sniffles-sv/"
                                                                "het-reads/bams/", help= "path to directory where "
                                                                "bam files from ngmlr (for all isolates) are located.")
parser.add_argument("-i", "--input", required = False, default = "/grp/valafar/data/variants/sniffles-sv/"
                                        "filtered_het_isos.txt", help = "Input file containing list of isolates to"
                                                                        "calculate the error rate from.")
parser.add_argument("--nproc", dest="number_cores", help="Enter the number of cores desired for processing, default"
                   "is 1.", required=False, default=1)

args = parser.parse_args()
nproc = int(args.number_cores)
args = parser.parse_args()
input_isos = args.input
bam_path = args.bam_path


def main():
    # mismatch_fraction_list = []
    isolate_list = get_unique_isos(input_isos)
    pool = mp.Pool(nproc)
    #partialfun = partial(calculate_E_individual_iso, list_to_append = mismatch_fraction_list)
    mismatch_fractions = pool.map(calculate_E_individual_iso, isolate_list)
    iso_count = len(mismatch_fractions)
    pool.close()
    now = datetime.now()
    elapsed = now - startTime
    E = mean(mismatch_fractions)
    print('time elapsed = '+ str(elapsed))
    print('error rate across ' + str(iso_count) + 'isolates in ' + str(input_isos) + ' = ' + str(E))




# align the query sequence to the reference sequence using OUR methods
# how many mismatches (among the positions that aligned) were there / how many total positions in the alignment
# store the fraction in a data structure
# calculate the average across ALL of the randomly selected alignments

def get_unique_isos(isolate_file):
    unique_list = []
    with open(isolate_file, "r") as read_obj:
        for line in read_obj:
            line = line.strip()
            if line != 'isolate':
                if line not in unique_list:
                    unique_list.append(line)
    return unique_list


def get_parsed_alignments(path_to_bams, isolate):
    """Takes a directory where bam file is located and an isolate name. Filters alignment out
    if mapping quality is less than 20. Returns a list (isolate specific) of lists (parsed alignments)."""
    output_list = []
    bam_file = pysam.AlignmentFile(path_to_bams + isolate + ".bam", "rb")
    for alignment in bam_file:
        # readname = alignment.qname
        ref_start = alignment.reference_start
        ref_stop = alignment.reference_end
        # qry_start = alignment.qstart
        # qry_stop = alignment.qend
        read_seq = alignment.query_alignment_sequence
        if alignment.is_reverse:
            strand = '-'
        else:
            strand = '+'
        mq = alignment.mapping_quality
        if mq > 20:
            alignment_list = [ref_start, ref_stop, strand, read_seq]
            output_list.append(alignment_list)
    return output_list


def count_mismatches(isolate, alignment_info):
    fasta = alignment.get_fasta(isolate)
    ref_start = alignment_info[0]
    ref_stop = alignment_info[1]
    strand = alignment_info[2]
    read_seq = alignment_info[3]
    region = (ref_start, ref_stop)
    ref_seq = alignment.get_sequence(region, fasta, strand)
    our_alignment = alignment.align(ref_seq, read_seq)
    read_ref_dict = alignment.make_read_ref_dict(our_alignment)
    mismatch_count = 0
    total_aligned_bases = 0
    for read_pos in read_ref_dict:
        total_aligned_bases += 1
        ref_pos = read_ref_dict[read_pos]
        read_base = read_seq[read_pos]
        ref_base = ref_seq[ref_pos]
        if read_base != ref_base:
            mismatch_count += 1
    return mismatch_count, total_aligned_bases


def calculate_E_individual_iso(isolate):
    # grab alignments and store
    # filter alignments (MQ 20)
    all_bam_alignments = get_parsed_alignments(bam_path, isolate)
    # take a random set of alignments from its bam file (how many?)
    sampled_alignments = random.sample(all_bam_alignments, 1000)
    # align the query sequence to the reference sequence using OUR methods
    for bam_alignment in sampled_alignments:
        # this will return the number of mismatches among the positions that aligned
        mismatches, aligned_bases = count_mismatches(isolate, bam_alignment)
        # get the total aligned bases (no gaps)
        # divide mismatches/total
        mismatch_fraction = mismatches/aligned_bases
    return(mismatch_fraction)


if __name__ == '__main__':
    main()
