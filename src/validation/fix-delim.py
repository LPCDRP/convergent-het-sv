file = '/Users/cassidyrobinhold/workspace/structural_variation/alignment_n_K_values.csv'

file_str ='isolate, sv_id, read_name, sv_type, bp_1, bp_2, alignment, A_region, B_region, n, k_A, k_B\n'
with open(file, 'r') as open_file:
    for line in open_file.readlines()[1:]:
        line = line.strip().split(',')
        Aregion1 = line[7].strip('(')
        Aregion2 = line[8].strip().strip(')')
        Bregion1 = line[9].strip('(')
        Bregion2 = line[10].strip().strip(')')
        Aregion = Aregion1+':'+Aregion2
        Bregion = Bregion1+':'+Bregion2
        region_list = [Aregion, Bregion]
        new_line = line[0:7] + region_list + line[11:]
        file_str += ','.join(new_line)+'\n'

with open('/Users/cassidyrobinhold/workspace/structural_variation/alignment_n_K_values2.csv', 'w') as out_file:
    out_file.write(file_str)
