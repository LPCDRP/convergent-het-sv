for iso in *.bam; do samtools depth -a $iso | awk -v iso="${iso%.bam}" '{sum+=$3} END {print iso,sum/NR}'; done
