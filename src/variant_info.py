#!/usr/bin/env python2.7

from pysam import VariantFile
import argparse

parser = argparse.ArgumentParser(description="""Parses sniffles output and creates tsv file with important 
                                                variant attributes.""")
parser.add_argument("-i", "--input", nargs='+', required=True, help="One or more vcf files to be parsed")
parser.add_argument("-o", "--output", required=True, help="Name of output tsv file")

args = parser.parse_args()
vcf_files = args.input
tsv_out = args.output


def create_header(tsv_name):
    header_list = ['ID', 'POS', 'END', 'SVLEN', 'SVTYPE', 'STRANDS', 'FILTER', 'PRECISE.IMPRECISE',
                   'ZMW', 'STD_start', 'STD_stop', 'Kurt_start', 'Kurt_stop', 'RE', 'AF', 'RNAMES']
    header_string = '\t'.join(header_list) + '\n'
    with open(tsv_name, 'w') as tsv_open:
        tsv_open.write(header_string)
    return tsv_open


def add_variant_info(vcf_name,tsv_name):
    iso_id = vcf_name[:-7]
    vcf_in = VariantFile(vcf_name)
    file_string = ''
    for rec in vcf_in.fetch():
        position = str(rec.pos)
        end = str(rec.stop)
        length = str(rec.info['SVLEN'])
        svtype = str(rec.info['SVTYPE'])
        strands = str(rec.info['STRANDS'][0])
        resolved = rec.filter.keys()[0]
        precise = list(rec.info.keys())[0]
        zmw = str(rec.info['ZMW'][0])
        std_start = str(rec.info['STD_quant_start'][0])
        std_stop = str(rec.info['STD_quant_stop'][0])
        kurt_start = str(rec.info['Kurtosis_quant_start'][0])
        kurt_stop = str(rec.info['Kurtosis_quant_stop'][0])
        reads = str(rec.info['RE'])
        allele_freq = str(rec.info['AF'][0])
        rnames = ','.join(rec.info['RNAMES'])
        info_list = [iso_id, position, end, length, svtype, strands, resolved, precise, zmw, std_start, std_stop,
                    kurt_start, kurt_stop, reads, allele_freq, rnames]
        rec_string = '\t'.join(info_list) + '\n'
        if int(position) > 1:
            file_string += rec_string
    with open(tsv_name, 'a') as tsv_open:
        tsv_open.write(file_string)
    return tsv_open


def write_all():
    create_header(tsv_out)
    for vcf in vcf_files:
        add_variant_info(vcf, tsv_out)


if __name__ == '__main__':
    write_all()
