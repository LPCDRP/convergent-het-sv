#!/usr/bin/env make -f
GENOMES ?= $(GROUPHOME)/data/genomes
ISOLATES := $(notdir $(basename $(wildcard $(GENOMES)/*.fasta)))
NPROC ?= 6

all: $(foreach isolate, $(ISOLATES), $(isolate).vcf.gz)

# INPUT ?= $(GROUPHOME)/data/depot/assembly/$(isolate)/filtering/filtered_subreads.fasta
# isolate ?= 1-0006
#REFERENCE := $(GENOMES)/1-0007.fasta
ngmlr := ngmlr
sniffles := sniffles

include vcf.mk
include vcf.mk

%.vcf: %.unsorted.vcf
	grep "#" $< > $@ && grep -v '^#' $< | sort -k2,2n >> $@ && rm $<

%.unsorted.vcf: bams/%.bam
	qsub \
	-cwd \
	-N sniff-$* \
	-pe smp $(NPROC) \
	-sync yes \
	-b yes \
	-j yes \
	$(sniffles) --mapped_reads $< --vcf $@ --threads $(NPROC) --genotype --min_support 6 --report_seq --num_reads_report 500

bams/%.bam: bams/%.unsorted.bam $(GENOMES)/%.fasta
	samtools sort \
		--output-fmt BAM \
		--reference $(word 2,$^) \
		-o $@ \
		$< && \
	samtools index $@

bams/%.unsorted.bam: $(GENOMES)/%.fasta $(GROUPHOME)/data/depot/assembly/%/error_correction/data/filtered_subreads.fasta
	qsub \
	-cwd \
	-N ngmlr-$* \
	-pe smp $(NPROC) \
	-sync yes \
	-b yes \
	-j yes \
	$(ngmlr) \
	   --reference $< \
	   --query $(word 2,$^) \
	   --output $@ \
	   --threads $(NPROC)

.DELETE_ON_ERROR:

.PRECIOUS: bams/%.bam

