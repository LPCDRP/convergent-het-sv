#!/usr/bin/env python3

import argparse
from Bio import SeqIO
import shutil
import os

parser = argparse.ArgumentParser(description="""Parses an embl file from RATT output
                                                and returns a TSV with important information.""")
parser.add_argument("-i", "--input", required=True, help="One embl file to be parsed")
parser.add_argument("-o", "--output", required=True, help="Name of output tsv file. Note that if tsv name already"
                                                          "exists, the file will be appended. This is intended to be"
                                                          "run separately on multiple embl files to create one"
                                                          "concatenated tsv with all embl information.")

args = parser.parse_args()
embl_file = args.input
tsv_out = args.output

# original ID line: ID                   Transfer1.LR026975.1.final ; ; ; ; ; 5269097 BP.
# edited to:ID                   Transfer1.LR026975.1.final; SV 1; circular; DNA; STD; PLN; 5269097 BP.

def edit_first_line(embl_file):
    open_embl = open(embl_file)
    line1 = open_embl.readline()
    line1_list = line1.strip().split(';')
    new_line1 = ';'.join([line1_list[0], 'SV 1', 'circular', 'DNA', 'STD', 'PLN', line1_list[-1]])+'\n'
    out_file = embl_file[0:-4]+'2.embl'
    open_out_file = open(out_file, mode='w')
    open_out_file.write(new_line1)
    shutil.copyfileobj(open_embl, open_out_file)
    return(out_file)


def feature_list(embl_file):
    rec_list = []
    with open(embl_file, 'r') as embl:
        for record in SeqIO.parse(embl, "embl"):
            for feature in vars(record)['features']:
                rec_list.append(feature)
    return rec_list


def create_header(tsv_name):
    """Creates the first line in the new file (desired column names)"""
    header_list = ['isolate', 'feature', 'start', 'end', 'strand', 'gene', 'locus_tag',
                   'ec_number', 'codon_start', 'protein_ID', 'db_xref',
                   'product', 'note']
    header_string = '\t'.join(header_list) + '\n'
    with open(tsv_name, 'w') as tsv_open:
        tsv_open.write(header_string)
    return tsv_open

def add_gene_info(records,tsv_name, isolate):
    """Gets gene info from each record in a given list, writes desired info to the mew tsv file."""
    file_string = ''
    for record in records:
        feature = record.type
        start = int(record.location.start)
        end = int(record.location.end)
        strand = record.strand
        qualifiers = record.qualifiers
        try:
            db_xref = qualifiers['db_xref']
        except:
            db_xref = 'NA'
        try:
            locus_tag = qualifiers['locus_tag']
        except:
            locus_tag = 'NA'
        try:
            gene = qualifiers['gene']
        except:
            gene = 'NA'
        try:
            ec_number = qualifiers['ec_number']
        except:
            ec_number = 'NA'
        try:
            codon_start = qualifiers['codon_start']
        except:
            codon_start = 'NA'
        try:
            protein_ID = qualifiers['protein_id']
        except:
            protein_ID = 'NA'
        try:
            product = qualifiers['product']
        except:
            product = 'NA'
        try:
            note = qualifiers['note']
        except:
            note = 'NA'
        info_list = [isolate, feature, start, end, strand, gene[0], locus_tag[0], ec_number, codon_start[0],
                     protein_ID, db_xref[0], product[0], note[0]]
        info_list = [str(i) for i in info_list]
        info_list = ['NA' if i == 'N' else i for i in info_list]
        rec_string = '\t'.join(info_list) + '\n'
        file_string += rec_string
    with open(tsv_name, 'a') as tsv_open:
        tsv_open.write(file_string)
    return tsv_open


modified_embl = edit_first_line(embl_file)
rec_list = feature_list(modified_embl)
if not os.path.exists(tsv_out):
    create_header(tsv_out)
isolate = embl_file.split('.')[0].split('/')[-1]
#print('Isolate '+isolate+' added\n')
add_gene_info(rec_list, tsv_out, isolate)
