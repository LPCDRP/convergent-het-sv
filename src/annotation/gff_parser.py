#!/usr/bin/env python3

import argparse
import sys

parser = argparse.ArgumentParser(description="""Parses a GFF file from Prodigal output
                                                and returns a TSV with important information.""")
# parser.add_argument("-i", "--input", required=False, help="One GFF file to be parsed")
parser.add_argument("-o", "--output", required=True, help="Name of output tsv file")

args = parser.parse_args()
gff_files = map(str.rstrip, sys.stdin)
tsv_out = args.output

def create_header(tsv_name):
    """Creates the first line in the new file (desired column names)"""
    header_list = ['isolate','ID', 'feature', 'start', 'end', 'score', 'strand', 'locus_tag', 'gene', 'product']
    header_string = '\t'.join(header_list) + '\n'
    with open(tsv_name, 'w') as tsv_open:
        tsv_open.write(header_string)
    return tsv_open


def add_gene_info(gff_name,tsv_name):
    """Gets annotation info from each line in GFF, writes desired info to the new tsv file."""
    file_string = ''
    problem_count = 0
    isolate = gff_name[:-4].split('/')[-1]
    with open(gff_name, 'r') as gff:
        for line in gff:
            if not line.startswith('#'):
                line_list = line.strip().split('\t')
                if len(line_list) != 9:
                    problem_count+=1
                    continue
                feature = line_list[2]
                start = line_list[3]
                end = line_list[4]
                score = line_list[5]
                strand = line_list[6]
                attributes = line_list[8].split(';')
                attribute_dict = {}
                for attribute in attributes:
                    category = attribute.split('=')[0]
                    value = attribute.split('=')[1]
                    attribute_dict[category] = value
                ID = attribute_dict['ID']
                try:
                    locus_tag = attribute_dict['locus_tag']
                except:
                    locus_tag = 'NA'
                try:
                    gene = attribute_dict['gene']
                except:
                    gene = 'NA'
                try:
                    product = attribute_dict['product']
                except:
                    product = 'NA'
                info_list = [isolate, ID, feature, start, end, score, strand, locus_tag, gene, product]
                rec_string = '\t'.join(info_list) + '\n'
                file_string += rec_string
        with open(tsv_name, 'a') as tsv_open:
            tsv_open.write(file_string)


def write_all():
    create_header(tsv_out)
    for file in gff_files:
        add_gene_info(file, tsv_out)


if __name__ == '__main__':
    write_all()