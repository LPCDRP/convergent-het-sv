# $GROUPHOME/data/variants/sniffles-sv/
#
# all isolates that should be used to build the tree
#   filtered_het_isos.txt
# isolates that passed assembly QC
#   original_isos.txt
# additional isolates used in this study that had failed assembly QC
#   het-new-isos.txt
# mapping of SVs to isolate IDs (for labeling the tree)
#   commonSV-isolate-lists.csv

VPATH = ${GROUPHOME}/data/depot/arrow ${GROUPHOME}/resources

ISOLATES := $(subst -unknown,, $(shell tail -n +2 ${GROUPHOME}/data/variants/sniffles-sv/filtered_het_isos.txt))

AWK = awk -F '	' -v OFS='	'

REFERENCE ?= ${GROUPHOME}/resources/H37Rv-NC_000962.3.fasta

.PHONY: all
all: RaxML_bipartitions.clinical-isolates

phyly: RAxML_bipartitions.clinical-isolates sv-table.csv
	./check-monophyly $^ > $@

RaxML_bipartitions.clinical-isolates: clinical-isolates.min4.phy
	$(RM) RAxML_*
	raxmlHPC \
	-s $< \
	-n clinical-isolates \
	-o Mcanettii \
	-m GTRGAMMA \
	-f a \
	-x 12345 -p 12345 \
	-N 100

# The SV table has duplicate rows and a stupid -unknown suffix on one of the isolate IDs
sv-table.csv: ${GROUPHOME}/data/variants/sniffles-sv/commonSV-isolate-lists.csv
	< $< tail -n +2 \
	| sort -n \
	| uniq \
	| sed s/\-unknown//g \
	> $@

clinical-isolates.min4.phy: clinical-isolates.vcf
	vcf2phylip.py -i $< --outgroup Mcanettii


# these VCFs have records where there's an N in the reference...
# see https://github.com/PacificBiosciences/pbbioconda/issues/325
#     https://github.com/samtools/bcftools/issues/1300
.vcf-corrections: 1-0019.vcf.gz 1-0079.vcf.gz 1-0102.vcf.gz 1-0137.vcf.gz 3-0120.vcf.gz 4-0041.vcf.gz SEA10470.vcf.gz
	for f in $^; \
	do \
	    bcftools norm --check-ref x -f ${REFERENCE} --output-type z $$f -o $$(basename $$f); \
	    tabix -p vcf $$(basename $$f); \
	done;
	touch $@

clinical-isolates.vcf: $(ISOLATES:%=%.vcf.gz) Mcanettii.vcf.gz .vcf-corrections
	bcftools merge --missing-to-ref $(filter-out .vcf-corrections, $^) > $@

# outgroups
%.vcf.gz: %.fasta
	mkdir -p $*
	dnadiff -p $*/out ${REFERENCE} $<
	mummer-snps2vcf --reference ${REFERENCE} $*/out.snps \
	| $(AWK) '!/^#/ {$$1 = "1"} $$0' \
	| vcf-set-sample --name $* \
	| bgzip > $@
	tabix -p vcf $@
