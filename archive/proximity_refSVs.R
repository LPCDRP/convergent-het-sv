library(data.table)
library(dplyr)

features <- fread(file = '/grp/valafar/data/variants/sniffles-sv/het/all-isolates-features.tsv')
SVs <- fread(file = '/grp/valafar/data/variants/sniffles-sv/het/het_SV_151.tsv')
# het SV's that appear in more than one isolate
SVs <- fread(file='~/workspace/structural_variation/het_common_SVs.csv')
#CDS's on both plus and minus strands
only_CDS <- features[features$Feature == 'CDS',]

# set key as SV start
SV_starts_dt <- SVs
setkey(SV_starts_dt, ID, POS)
# Make CDS df with extra column as CDS stop
rollingjoin_start <- data.table(only_CDS, variant_start = only_CDS$Stop)
# set key as that column
setkey(rollingjoin_start, Isolate, variant_start)
# get nearest CDS stop to each variant start
nearest_CDSstop_SVstart <- rollingjoin_start[SV_starts_dt, roll = "nearest"]
# difference between CDS stop and SV start
nearest_CDSstop_SVstart$diff <- abs(nearest_CDSstop_SVstart$variant_start-nearest_CDSstop_SVstart$Stop)
# Make CDS df with extra column as CDS start
rollingjoin_start <- data.table(only_CDS, variant_start = only_CDS$Start)
# set key as that column
setkey(rollingjoin_start, Isolate, variant_start)
# get nearest CDS start to each variant start
nearest_CDSstart_SVstart <- rollingjoin_start[SV_starts_dt, roll = "nearest"]
# difference between CDS stop and SV start
nearest_CDSstart_SVstart$diff <- abs(nearest_CDSstart_SVstart$variant_start-nearest_CDSstart_SVstart$Stop)
# Closest CDS regardless of orientation
nearest_CDS_SVstart <- rbind(nearest_CDSstop_SVstart, nearest_CDSstart_SVstart) %>% 
  setorder(Isolate, variant_start, diff)
nearest_CDS_SVstart <- nearest_CDS_SVstart[!duplicated(nearest_CDS_SVstart, by = c("Isolate", "variant_start")),]

# set key as SV stop
SV_stops_dt <- SVs
setkey(SV_stops_dt, ID, END)
# Make CDS df with extra column as CDS start
rollingjoin_stop <- data.table(only_CDS, variant_stop = only_CDS$Start)
# set key as that column
setkey(rollingjoin_stop, Isolate, variant_stop)
# get nearest CDS start to each variant stop
nearest_CDSstart_SVstop <- rollingjoin_stop[SV_stops_dt, roll = "nearest"]
# difference between CDS start and SV stop
nearest_CDSstart_SVstop$diff <- abs(nearest_CDSstart_SVstop$variant_stop-nearest_CDSstart_SVstop$Start)
# Make CDS df with extra column as CDS stop
rollingjoin_stop <- data.table(only_CDS, variant_stop = only_CDS$Stop)
# set key as that column
setkey(rollingjoin_stop, Isolate, variant_stop)
# get nearest CDS stop to each variant stop
nearest_CDSstop_SVstop <- rollingjoin_stop[SV_stops_dt, roll = "nearest"]
# difference between CDS stop and SV stop
nearest_CDSstop_SVstop$diff <- abs(nearest_CDSstop_SVstop$variant_stop-nearest_CDSstop_SVstop$Stop)
# Closest CDS regardless of orientation
nearest_CDS_SVstop <- rbind(nearest_CDSstart_SVstop, nearest_CDSstop_SVstop) %>% 
  setorder(Isolate, variant_stop, diff)
nearest_CDS_SVstop <- nearest_CDS_SVstop[!duplicated(nearest_CDS_SVstop, by = c("Isolate", "POS")),]

# clean up
nearest_CDS_SVstop <- rename(nearest_CDS_SVstop, CDSstart = Start, CDSstop = Stop, variant_start = POS)
nearest_CDS_SVstop <- nearest_CDS_SVstop[, c(9, 1:7, 10, 8, 11:23)] %>% setorder(SV_ID)


# fwrite(nearest_CDSstop_SVstart, file = '/grp/valafar/data/variants/sniffles-sv/het/proximity_SVstart.csv', row.names = FALSE, col.names = TRUE)
# fwrite(nearest_CDSstart_SVstop, file = '/grp/valafar/data/variants/sniffles-sv/het/proximity_SVstop.csv', row.names = FALSE, col.names = TRUE)
fwrite(nearest_CDS_SVstart, file = '/grp/valafar/data/variants/sniffles-sv/het/proximity_commonSV_start.csv', row.names = FALSE, col.names = TRUE)
fwrite(nearest_CDS_SVstop, file = '/grp/valafar/data/variants/sniffles-sv/het/proximity_commonSV_stop.csv', row.names = FALSE, col.names = TRUE)

