# import things

import argparse
# from Bio.Blast.Applications import NcbiblastnCommandline
import os.path
# from Bio.Emboss.Applications import NeedleCommandline
from Bio import pairwise2
from Bio import SeqIO

parser = argparse.ArgumentParser(description="""Aligns flanking regions of structural variants and returns file with
                                                alignment info.""")
parser.add_argument("-i", "--input", required = True, help = "Input in csv format. Each row should represent each read"
                                                             "that supports each structural variant.")
parser.add_argument("-o", "--output", required = False, default='identity_output.tsv', help = "user defined name for output"
                                                                        " file. Default is identity_output.tsv")
parser.add_argument("-e", "--error_output", required = False, default='error_reads.csv', help = "lines from the input "
                                                                        "file where the corresponding regions could not"
                                                                        " be run through the alignment algorithm, likely"
                                                                        " because the read mapping does not overlap with"
                                                                        " at least one SV breakpoint.")
args = parser.parse_args()
in_csv = args.input
output_name = args.output
error_output_name = args.error_output


def align(Astring, Bstring, match = 1, mismatch = 0, gapopen = 0, gapextend = 0):
    """Takes 2 strings, performs global alignment. Can specify scores for match, mismatch, gapopen, and gap extend.
     Default values are 1, 0, 0, and 0, respectively. Returns a float: the highest score/length of sequence."""
    best_score = pairwise2.align.globalms(Astring, Bstring, match, mismatch, gapopen, gapextend, score_only=True)
    identity = best_score/len(Astring)
    return identity


def ab_regions(read_start, read_stop, breakpoint1, breakpoint2):
    """Takes read start, read stop (alignments to consensus) and breakpoint1 and breakpoint2 (output for each
    variant from Sniffles). Returns a1,a2,b1,and b2 indexes in the consensus genome in the form of a tuple
    with (beginning,end) of region. Output is therefore a tuple of tuples."""
    if int(read_start) < int(breakpoint1):
        a1 = (read_start, breakpoint1)
        a1length = int(breakpoint1) - int(read_start)
        a2 = (breakpoint1, read_stop)
        a2length = int(read_stop)-int(breakpoint1)
        b1 = (breakpoint2, str(int(breakpoint2)+a1length))
        b2 = (str(int(breakpoint2)-a2length), breakpoint2)
    elif int(read_start) > int(breakpoint1):
        a1 = (breakpoint2, read_stop)
        a1length = int(read_stop) - int(breakpoint2)
        a2 = (read_start, breakpoint2)
        a2length = int(breakpoint2)-int(read_start)
        b1 = (str(int(breakpoint1)-a1length), breakpoint2)
        b2 = (breakpoint1, str(int(breakpoint1)+a2length))
    return (a1, a2, b1, b2)


def get_fasta(isolate_name):
    if os.path.isfile('/grp/valafar/data/genomes/'+isolate_name+'.fasta'):
        fasta = '/grp/valafar/data/genomes/'+isolate_name+'.fasta'
    elif os.path.isfile('/grp/valafar/data/variants/sniffles-sv/het/new_isos/'+isolate_name+'.fasta'):
        fasta = '/grp/valafar/data/variants/sniffles-sv/het/new_isos/'+isolate_name+'.fasta'
    else:
        print('Fasta for isolate ' + isolate_name + ' not found in genomes/ directory or new_isos/ directory')
    return fasta


def get_sequence(region, fasta):
    """Takes a tuple with indexes for the A or B region and a fasta file name. Returns the desired sequence
    from the fasta as a string."""
    record = list(SeqIO.parse(fasta, "fasta"))[0]
    start = int(region[0])
    stop = int(region[1])
    if start < stop:
        ab_seq = str(record.seq)[start:stop]
    else:
        # might need to reverse/compliment or do something else here
        ab_seq = str(record.seq)[stop:start]
    return ab_seq


def iterate(input_file):
    """Takes csv file with location info for each read supporting each variant. Aligns A1&B1 regions and
    A2&B2 regions. Returns 2 lists: the first has SV/read/alignment info with all blast results, the second has
    the lines in the csv input that could not be run through blast."""
    all_results = []
    error_reads = []
    with open(input_file) as bam_locations:
        for line in bam_locations:
            if not line.startswith('ISOLATE'):
                line = line.strip().split(',')
                isolate = line[0]
                sv_id = line[1]
                read_name = line[2]
                sv_type = line[3]
                read_map_start = line[4]
                read_map_stop = line[5]
                bp1 = line[8]
                bp2 = line[9]
                # read_length = line[7]
                # map_length = str(int(line[5])-int(line[4]))
                mapping_info = [isolate, sv_id, read_name, sv_type, bp1, bp2]
                ab_loc = ab_regions(read_map_start, read_map_stop, bp1, bp2)
                fasta_file = get_fasta(isolate)
                a1 = get_sequence(ab_loc[0], fasta_file)
                a2 = get_sequence(ab_loc[1], fasta_file)
                b1 = get_sequence(ab_loc[2], fasta_file)
                b2 = get_sequence(ab_loc[3], fasta_file)
                try:
                    score_1 = align(a1, b1)
                except:
                    score_1 = 'NA'
                    error_reads.append(','.join(line)+','+'1')
                try:
                    score_2 = align(a2, b2)
                except:
                    score_2 = 'NA'
                    error_reads.append(','.join(line)+','+'2')
                file_string1 = '\t'.join(mapping_info) + '\t1\t' + '\t' + str(score_1)
                file_string2 = '\t'.join(mapping_info) + '\t2\t' + '\t' + str(score_2)
                all_results.append(file_string1)
                all_results.append(file_string2)
    return all_results, error_reads


def write_output(align_list):
    header = 'ISOLATE\tSVID\tREAD_NAME\tSV_TYPE\tBP_1\tBP_2\tAB\tglobal_score\n'
    align_string = '\n'.join(align_list)
    file_string = header + align_string
    with open(output_name, 'w') as out_file:
        out_file.write(file_string)


def write_error_reads(error_reads):
    header = 'ISOLATE,SVID,READ_NAME,SV_TYPE,REF_START,REF_STOP,QRY_START,QRY_STOP,BP_1,BP_2,Map_QUAL,AB'
    error_string = '\n'.join(error_reads)
    file_string = header + error_string
    with open(error_output_name, 'w') as out_file:
        out_file.write(file_string)


if __name__ == "__main__":
    align_list, error_reads = iterate(in_csv)
    write_output(align_list)
    write_error_reads(error_reads)
