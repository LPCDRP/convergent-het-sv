This repository contains the code used for all analysis and data visualization for the study titled "Genome structure heterogeneity in Mycobacterium tuberculosis is constitutive and recapitulates events in its macroevolution"

# **Software Requirements**

## **OS requirements**
The code and software in this repository was developed, tested, and executed in a Linux (Debian) environment.

## **Software Dependencies**
*  Python version 3.6
*  R version 3.6.1
*  Ngmlr version
*  Sniffles version
*  Samtools version 1.9
*  Minimap2 version 2.15
*  Arrow version 2.3.3
*  RaXML version 8.2.12
*  MUMmer version 3.23
*  ETE Toolkit version 3.1.2

# **Scripts**

## **Validation**

*  src/validation/align_loop.sh - delete (archive)
*  src/validation/alignment_validation.py - contains alignment functions used in validation.py
*  src/validation/calculate_error_rate.py - calculates mismatch rate of all alignments across all isolates using secondary alignment method
*  src/validation/filter-SVs.R - calculates probability of sequence belonging to observed or alternative alignment regions ("p" and q-values)
*  src/validation/findReadMapLocation.py - gets alignment positions from .bam files. To be imported into validation.py
*  src/validation/fix-delim.py - alters alignment data file for compatibility with downstream script
*  src/validation/investigate_bam_alignments.R - delete (archive)
*  src/validation/melt_reads.R - takes parsed sniffles output, converts comma separated list of readnames from sniffles into individual rows to be used for validation.
*  src/validation/validation.py - performs alignment validation for all isolates. Takes in parsed SVs from Sniffles and .bam files from ngmlr, outputs "n" an "k" values for each "A" and "B" region

## **Annotation**
*  src/annotation/gff_parser.py - parses prokka output and returns tsv
*  src/annotation/2021 promoter_effect.R - establishes where 5' UTR, promoter, and CDS regions are for each annotated gene. Adds operon to each gene. Finds overlap between validated, clustered SVs and these regions. Writes output with each instance of SV/feature overlap, whether partial or complete. 
*  src/annotation/prokka_promoter_effect.R - takes parsed prokka annotation, finds overlap between validated, clustered SVs and CDSs. Writes output for each overlap.
*  src/annotation/ratt_embl_parser.py - parses embl output from RATT and returns tsv
*  src/annotation/sv_res_genes.R - delet (archive, already in sv_summary_statistics)


## **Other Analyses**

*  src/SV_origin_hyp_test.R - tests hypothesis 1 (SVs are present in all isolates at constant frequency and simply are not supported by the reads that happened to be sequenced). Creates Figure 8
*  src/cass_figures.R - performs calculations and creates visualizations for Figure 2
*  src/estimate_abundance.R - creates file with one row for each SV conatining: number of SV supporting reads and average overall coverage in isolate
*  src/get_iso_length.py - calculates length of consensus sequence for each isolate
*  src/het-SV-clustering.R - performs K-NN clustering on SVs
*  src/het-ngmlr-sniffles.mk - runs ngmlr and sniffles on each isolate using read sequences and consensus genome as input
*  src/het_SV_feat_interpreter.R - creates figure 7A
*  src/identical_genes_investigation.R - delete (archive)
*  src/iso_avg_cov.sh - get average coverage per isolate from ngmlr .bam files
*  src/isolate_list_reconciliation.R - delete (archive)
*  src/sv_summary_statistics.R - creates Tables 1 and 2. Calculates summary statistics (for example: how many SVs of each type were detected)
*  src/variant_info.py - parses Sniffles output into tsv
*  src/blast_sv_het.py - BLASTs the SV content and genomic content to establish which isolates had the requisite sequence context for an SV to occur. Run on deletions and duplications only.
*  src/blast_inversions.py - BLASTs the SV content and genomic content to establish which isolates had the requisite sequence context for an SV to occur. Run on inversions (need to blast from regions of both breakpoints)
*  src/blastout-plotter.R - takes in output from blast_sv_het.py to generate plots of identity and coverage of best hits for SVs
 

# **Source Code for Figures and Tables**
Figure 1 was created using (sam fill).
Figure 4 B,C,D & Figure 6 were created using Microsoft Powerpoint.
Figure 7B was created using RaXML.
All other figures were created in R using the ggplot2 package and the.
Source code used to create figures and tables can be found in the following scripts
*  Figure 2:
    A,B,C: src/cass_figures.R 
*  Figure 3: src/het-SV-clustering.R
*  Figure 4:
    E,F,G,H,I: src/SV_true_probability.R
*  Figure 5: src/het_SV_feat_interpreter.R
*  Figure 7:
    A: src/het_SV_feat_interpreter.R
    B: src/phylo/MAKEFILE
*  Figure 8:
    A,B: src/SV_origin_hyp_test.R

Tables:
*  Table 1: src/sv_summary_statistics.R
*  Table 2: src/sv_summary_statistics.R


